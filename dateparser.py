__author__ = 'alastair'
from datetime import datetime, date
from variables import base_misspelling_dict, correct_month_spellings, date_order_dict


class DateParser(object):

    def __init__(self, default=date.today(), str_order="day,month,year", misspelling_dict=None):

        # keys for possible month mis-spellings.
        month = base_misspelling_dict

        # add all the user added misspellings, check for duplicates then add it to the main misspelling dictionary
        # print(month_keys)
        if isinstance(misspelling_dict, dict):
            for key in misspelling_dict:
                if key not in correct_month_spellings:
                    raise KeyError('key in misspelling_dict misspelled.')
                month[key] = list(set(month[key] + misspelling_dict[key]))
        elif misspelling_dict is not None:
            raise ValueError("misspelling_dict must be a dictionary")

        # quick script to assign the misspellings as keys and actual spellings as values.
        self.month_dict = {}
        for key in correct_month_spellings:
            for misspelling in month[key]:
                self.month_dict[misspelling.lower()] = key

        # will check the default to see if it's a date or datetime object
        if isinstance(default, datetime):
            self.default = default.date()
        elif not isinstance(default, date):
            raise ValueError(str(type(self.default)) + " not a valid type for default")
        else:
            self.default = default

        # splits date order by arbitrary separator and checks for valid input of separator, returns a list with
        # 'year' 'month' and 'day' using regex
        import re
        self._date_order = [date_order_dict[part.strip().lower()] for part in re.findall(r"[\w']+", str_order)
                            if (part in [key for key in date_order_dict])]

        if len(self._date_order) != 3:
            raise ValueError("invalid input for str_order")

    def parse(self, raw_date=''):
        """
        parse method will return a date object. It takes a raw string input and converts it to a date based on the
        parameters initialized into the DateParser object.
        """

        if raw_date == '':
            return self.default

        date_dict = {'month': '', 'day': '', 'year': '', }

        import re
        date_list = [self.month_dict.get(part.strip().lower(), part.strip().lower())
                     for part in re.findall(r"[\w']+", raw_date) if part != '']
        
        # for part in date_list:
        #     if part.isdigit() and len(part) == 4:
        #         date_dict['year'] = part
        #     elif part.isalpha():
        #         date_dict['month'] = part
        #     elif part.isdigit() and len(part) == 2:
        #         if int(part) in range(0, 32):
        #             date_dict['day'] = part
        print(date_list)

        return date(year=date_dict['year'], month=date_dict['month'], day=date_dict['day'])