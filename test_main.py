__author__ = 'Alastair'
from dateparser import DateParser
from datetime import date, datetime, timedelta
from functools import reduce
from test_functions import *
import pprint

prettyprint = pprint.PrettyPrinter(indent=4).pprint

print("Constructor Tests:\n-----------------------------")
# test __init__
misspelling_dict = {
    'january': ['test', 'adf', 'add'],
    'february': ['get', 'dad', 'dafadf'],
    'march': ['daf']
}
str_orders = ["y.mon.d", 'mon.y.d', 'year,month.day', 'day,y/month', 'y m d']
print("Test date order construction:")
for str_order in str_orders:
    parser = DateParser(
        default=datetime.now(),
        str_order=str_order,
        misspelling_dict=misspelling_dict
    )
    print("str_order='" + str_order + "' --> " + str(parser._date_order))
print("\nTest misspelling_dict:\n\nuser-added misspelling_dict: ")
prettyprint(misspelling_dict)
parser = DateParser(
    default=datetime.now(),
    str_order="y,m,d",
    misspelling_dict=misspelling_dict
)
print("generated misspelling dictionary: ")
prettyprint(parser.month_dict)

# test speed of __init__

average = []
tests = 5
quantity = 10000
print("\nTest speed of init, " + str(quantity) + " tests each: ")
for i in range(tests):
    average.append(time_init(quantity))
    print(str(i + 1) + ": " + str(average[i].total_seconds()) + " sec.")
print("average: " + str(reduce(lambda x, y: x + y, average).total_seconds() / len(average)) + " sec.")
