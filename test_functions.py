from dateparser import DateParser
from datetime import date, datetime, timedelta


def time_init(quantity):
    start = datetime.now()
    parser_list = []
    for x in range(quantity):
        parser_list.append(DateParser(
            default=datetime.now(),
            str_order="mon.y.d",
            misspelling_dict={
                'january': ['test', 'adf', 'add'],
                'february': ['get', 'dad', 'dafadf'],
                'march': ['daf']
            }
        )
    )
    delta = datetime.now() - start
    return delta