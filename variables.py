__author__ = 'Alastair'

base_misspelling_dict = {
    'january': ['JANURY', 'JANUARYY', 'JANAURY', 'JNUARY', 'JAN', 'JANUARY,'],
    'february': ['DEBRUARY', 'FEBRARY', 'FEBRUARYY', 'FEBRAURY', 'FEBUARY', 'FEB', 'FEBRUARY,'],
    'march': ['MAR', 'MARCH,'],
    'april': ['APR', 'APRIL,'],
    'may': ['MAY,'],
    'june': ['JUN', 'JUNE,'],
    'july': ['JULYY', 'JUL', 'JULY,'],
    'august': ['AUGSUT', 'AUG', 'AUGUST,'],
    'september': ['SEPT', 'SEP', 'SEPTEMBER,'],
    'october': ['OCOTBER', 'OCTO', 'OCT', 'OCTOBER,'],
    'november': ['NOVEMEBR', 'NOV', 'NOVEMBER,'],
    'december': ['DECENBER', 'DECEMEBR', 'DEECEMBER', 'DEC', 'DECEMBER', ]
}

correct_month_spellings = [key for key in base_misspelling_dict]

date_order_dict = {
    'y': 'year', 'm': 'month', 'd': 'day', 'ye': 'year', 'mon': 'month', 'da': 'day',
    'year': 'year', 'month': 'month', 'day': 'day',
}